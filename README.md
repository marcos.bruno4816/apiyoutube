# Projeto YoutubeAPI

Projeto criado para entregar o tempo necessario, que o usuario levaria em dias, para visualizar uma quantidade de videos, a partir de uma pesquisa.

## Instalação
- [Instalar a versao 10.0 do node ou superior](https://nodejs.org/pt-br/download/)
- [Instalar o Yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable)


## Depois da Instalação
- Faça o download do projeto [aqui](https://gitlab.com/marcos.bruno4816/apiyoutube.git) e copei em uma pasta de sua preferencia.
- Vá ate a pasta onde o projeto está localizado, execute no prompt de comando a linha "yarn start".
- O projeto ficará disponível na porta 3333, para evitar conflitos com outros sistemas.
- Para acessar o projeto, basta abrir o browser a sua escolha, e digitar a url "localhost:3333"

