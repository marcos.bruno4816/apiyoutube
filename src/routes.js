import { Router } from 'express';

import UserController from './app/controllers/UserController';
import YoutubeController from './app/controllers/YoutubeController';

const routes = new Router();

routes.get('/', (req, res) => {
  res.render('pages/home');
});

routes.get('/about', (req, res) => {
  res.render('pages/about');
})

routes.get('/videos', YoutubeController.index);

// routes.get('/videosTeste', YoutubeController.index);

export default routes;
