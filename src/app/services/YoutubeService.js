class YoutubeService {

  constructor(maxResult, searchText){
    this.arrSearchList = {
      "part": 'snippet',
      "type": 'video',
      "order": 'relevance',
      "maxResults": maxResult,
      "q": searchText
    };
    this.arrSearchVideo = {
      "part": 'contentDetails'
    };
  }

  countDaysToSeeVideos(videos, time) {
  
    function nextDay(){
      (day === 6) ? day = 0: day++;
      countDays++;
      countTime = 0;      
    }
  
    let countDays = 1;
    let day = 0;
    let countTime = 0;
    let countVideos = 1;
    let finished = false;
    
    const videosDuration = videos.map((video) => {
      return parseInt(video.duration, 10);
    });
    const videosDurationLength = videosDuration.length;
  
    do {
      let dayTime = time[day];
      let videoTime = videosDuration[countVideos - 1];      
      
      if (videoTime <= dayTime) {
        console.log(`VideoTime ${videoTime} e menor do que dayTime ${dayTime}`);
        if ((countTime + parseInt(videoTime, 10)) > dayTime) {
          console.log(`A soma dos tempos dos videos anteriores (${countTime + videoTime}) fica maior do que ${dayTime}`);
          nextDay();
          continue;
        } else {
          console.log(`Adicionando ${videoTime} ao contador ${countTime}`);
          countTime +=  parseInt(videoTime, 10);
          countVideos ++
        }
      } else {
        for (let weekDays = 0; weekDays < 7; weekDays++){
  
          if (videoTime < time[weekDays]) {
            nextDay();
            break;
          } else {
            if (weekDays === 6) {
              console.log(`O video sera ignorado por seu tempo ser maior que todos os disponiveis ${videoTime}`);
              countVideos++
            }
          }
        }
      }
  
      if (countVideos > videosDurationLength) {
        console.log(`Todos os videos foram analisados`);
        finished = true;
      }
  
    } while (!finished)
  
    return countDays;
    
  };
  
  createVideoObject(item, duration) {
    const { videoId } = item.id;
    const {title, description} = item.snippet;
    
    return {
      videoId,
      title,
      description,
      duration,
    }
  }

}


export default YoutubeService;

