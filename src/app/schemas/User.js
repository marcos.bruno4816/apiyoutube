import mongoose from 'mongoose';
// OAUTH2
const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
    },
    hoursPerDay: {
      type: [Number],
      required: true,
      default: [60,60,60,60,60,60,60]
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('User', UserSchema);
