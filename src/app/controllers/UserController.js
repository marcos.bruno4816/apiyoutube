import * as Yup from 'yup';
import User from '../schemas/User';

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(),
      hoursPerDay: Yup.Array().of(yup.number())
        .required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const userExists = await User.findOne({ where: { email: req.body.email } });

    if (userExists) {
      res.status(400).json({ error: 'User already exists.' });
    }
    const { id, email, hoursPerDay } = await User.create(req.body);

    return res.json({ id, email, hoursPerDay });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      hoursPerDay: Yup.Array().of(yup.number())
        .required(),
      email: Yup.string().email()
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const { email } = req.body;

    const user = await User.findByPk(req.userId);

    if (email !== user.email) {
      const userExists = await User.findOne({ where: { email } });

      if (userExists) {
        res.status(400).json({ error: 'User already exists.' });
      }
    }

    const { id, hoursPerDay } = await user.update(req.body);

    return res.json({ id, email, hoursPerDay });
  }
}

export default new UserController();
