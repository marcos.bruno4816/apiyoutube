import {google} from 'googleapis';

import YoutubeService from '../services/YoutubeService';

class YoutubeController {
  async index(req, res) {

    const arrVideos = []

    const { searchText, maxResult, weekTime, pageToken } = req.query;

    const ytService = new YoutubeService(maxResult, searchText);

    const arrWeek = weekTime.split(',');

    const youtube = google.youtube({
      version: 'v3',
      auth: process.env.YOUTUBE_KEY
    });

    if (pageToken != '') {
      ytService.arrSearchList.pageToken = pageToken;
    }

    const result = await youtube.search.list(ytService.arrSearchList);

    if (result.status === 200) {
      const { nextPageToken, prevPageToken, items } = result.data;

      for (let item of items) {
        ytService.arrSearchVideo.id = item.id.videoId
        let videoDetail = await youtube.videos.list(ytService.arrSearchVideo);

        const duration = videoDetail.data.items[0].contentDetails.duration;

        arrVideos.push(ytService.createVideoObject(item, duration.substring(duration.indexOf('PT') + 2, duration.indexOf('M'))))
      }

      const daysToSeeVideos = ytService.countDaysToSeeVideos(arrVideos, arrWeek);

      const response = {arrVideos, daysToSeeVideos, nextPageToken, prevPageToken};

      res.status(200).json(response);

    } else {
      res.json({ errorMsg: "Erro na requisicao" });
    }    
  }  
}
export default new YoutubeController();