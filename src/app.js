import 'dotenv/config';

import express from 'express';
import expressLayouts from 'express-ejs-layouts';
import bodyParser from 'body-parser';
import path from 'path';
import routes from './routes';

import './database';

class App {
  constructor() {
    this.server = express();
    this.middlewares();
    this.views();
    this.routes();
  }

  middlewares() {
    this.server.use(express.json());
    this.server.use(bodyParser.urlencoded({extended: true}));
    this.server.use(expressLayouts);
  }

  routes() {
    this.server.use(routes);
  }

  views() {
    this.server.set('view engine', 'ejs');
    this.server.set('views', './src/app/views');
    this.server.use(express.static(path.resolve(__dirname, 'public')));
  }
}

export default new App().server;
