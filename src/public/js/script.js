window.onload = function() {
  const ytForm = document.getElementById('youtubeForm');
  const keywordInput = document.getElementById('searchInput');
  const maxresultInput = document.getElementById('maxresultInput');
  const monday = document.getElementById('monday');
  const tuesday = document.getElementById('tuesday');
  const wednesday = document.getElementById('wednesday');
  const thursday = document.getElementById('thursday');
  const friday = document.getElementById('friday');
  const saturday = document.getElementById('saturday');
  const sunday = document.getElementById('sunday');
  const videoList = document.getElementById('videoListContainer');
  const videoTime = document.getElementById('VideoTimeSpend');
  const errorMsg = document.getElementById('errorMsg');
  var pageToken = '';
  
  ytForm.addEventListener('submit', e => {
      e.preventDefault();
      errorMsg.innerHTML = ""
      execute(setBodyVideos);
  });
  
  function paginate(e, obj) {
      e.preventDefault();
      pageToken = obj.getAttribute('data-id');
      execute();
  }
  
  function execute(callback) {

    const weekTime = [
      monday.value, 
      tuesday.value, 
      wednesday.value, 
      thursday.value, 
      friday.value, 
      saturday.value, 
      sunday.value
    ];

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText, videoList, videoTime);
    }
    xmlHttp.open("GET", '/videos?searchText=' + keywordInput.value + '&maxResult=' + maxresultInput.value + '&weekTime=' + weekTime, true); // true for asynchronous 
    xmlHttp.send(null);
  }
  
}   

    function setBodyVideos(response, videoList, videoTime) {

      response = JSON.parse(response)

      if (response.errorMsg != null) {
        errorMsg.innerHTML = response.errorMsg

      } else {
      const listItems = response.arrVideos;

      if (listItems) {
          let output = '<h4>Videos</h4><ul>';

          listItems.forEach(item => {
              const videoId = item.videoId;
              const videoTitle = item.title;

              output += `
                   <li><img src="http://i3.ytimg.com/vi/${videoId}/hqdefault.jpg" /></a><p>${videoTitle}</p></li>
               `;
          });
          output += '</ul>';

          if (response.prevPageToken) {
              output += `<br><a class="paginate" href="#" data-id="${response.prevPageToken}" onclick="paginate(event, this)">Prev</a>`;
          }

          if (response.nextPageToken) {
              output += `<a href="#" class="paginate" data-id="${response.nextPageToken}" onclick="paginate(event, this)">Next</a>`;
          }

          videoList.innerHTML = output;
          videoTime.innerHTML = `<h4> Tempo para assistir os videos: ${response.daysToSeeVideos} dia(s)`
        }
      }
    }

